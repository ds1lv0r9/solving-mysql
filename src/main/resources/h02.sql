
use solving_mysql;

insert into clients (first_name, last_name) values ("Lisa", "Simpson"), ("Homer", "Simpson"), ("Ned", "Flanders");
insert into clients (first_name, last_name) values ("Kenny", "McCormick"), ("Sirius", "Black"), ("Boromir", "son of Denethor II");
insert into offers (name, client_id) values ("Offer 1", 1), ("Offer 2", 2), ("Offer 3", 3);

truncate table continents;
insert into continents (name) values ("Europe"), ("Basia"), ("Africa"), ("Australia"),  ("North America"), ("South America"), ("Antarctica"), ("Middle Earth");
insert into countries (name) values
	("Poland"), ("Spain"), ("Japan"), ("China"), ("Australia"), ("Brazil"), ("Argentina"), ("United States"), ("Canada"), ("Egypt"), ("Libya"), ("Amestris");
insert into continent_countries (continent_id, country_id) values
	(1, 1), (1, 2),  (2, 3),  (2, 4),  (4, 5),  (6, 6),  (6, 7),  (5, 8),  (5, 9),  (3, 10),  (3, 11);
insert into cities (name, country_id) values
	("Warsaw", 1), ("Poznań", 1), ("Barcelona", 2), ("Madrid", 2), ("Tokyo", 3), ("Yokohama", 3), ("Beijing", 4), ("Shenzhen", 4),
    ("Sydney", 5), ("Melbourne", 5), ("Rio de Janeiro", 6), ("São Paulo", 6), ("Buenos Aires", 7), ("Rosario", 7),
    ("New York", 8), ("Chicago", 8), ("Toronto", 9), ("Montreal", 9), ("Cairo", 10), ("Alexandria", 10), ("Tripoli", 11), ("Benghazi", 11),
    ("King's Landing", 12);
insert into hotels (name, city_id) values
	("Hotel Warsaw", 1), ("Hotel Poznań", 2), ("Hotel Barcelona", 3), ("Hotel Madrid", 4), ("Hotel Tokyo", 5), ("Hotel Yokohama", 6),
    ("Hotel Beijing", 7), ("Hotel Shenzhen", 8), ("Hotel Sydney", 9), ("Hotel Melbourne", 10), ("Hotel Rio de Janeiro", 11), ("Hotel São Paulo", 12),
    ("Hotel Buenos Aires", 13), ("Hotel Rosario", 14), ("Hotel New York", 15), ("Hotel Chicago", 16), ("Hotel Toronto", 17), ("Hotel Montreal", 18),
    ("Hotel Cairo", 19), ("Hotel Alexandria", 20), ("Hotel Tripoli", 21), ("Hotel Benghazi", 22),
    ("Hotel California", 22), ("The Dragonfly Inn", 22);
insert into rooms (name, price, maximum_guests, availible, hotel_id, offer_id) values
    ("Room 1", 100, 1, true, 1, 1), ("Room 2", 120, 2, true, 1, 1), ("Room 1", 90, 1, true, 2, 1), ("Room 2", 115, 1, true, 2, 1),
    ("Room 1", 105, 1, true, 3, 1), ("Room 2", 125, 2, true, 3, 1), ("Room 1", 110, 1, true, 4, 1), ("Room 2", 130, 1, true, 4, 1),
    ("Room 1", 120, 1, true, 5, 1), ("Room 2", 140, 2, true, 5, 1), ("Room 1", 115, 1, true, 6, 1), ("Room 2", 135, 1, true, 6, 1),
    ("Room X", 115, 1, true, 6, 1), ("Room Y", 135, 1, true, 6, 1);
insert into offer_rooms (offer_id, room_id) VALUES ('1', '1'), ('1', '2'), ('2', '3'), ('3', '4'), ('3', '5');

update continents set name = "Asia" where id = 2;
update continents set name = "Atlantis‎" where id = 8;
update countries set name = "Poland" where id = 1;
update countries set name = "Oceania" where id = 12;
update cities set name = "Chicago" where id = 16;
update cities set name = "Valhalla" where id = 23;
update hotels set name = "The Fhloston Paradise Hotel" where id = 23;
update hotels set name = "Fawlty Towers" where id = 24;
update rooms set price = 42 where id = 13;
update rooms set maximum_guests = 6 where id = 14;

delete from continents where id = 8;
delete from countries where id = 12;
delete from cities where id = 23;
delete from hotels where id = 23;
delete from hotels where id = 24;
delete from rooms where maximum_guests = 6;
delete from rooms where price = 42;
delete from clients where id = 4;
delete from clients where id = 5;
delete from clients where id = 6;

alter table clients add age integer;
alter table clients add email varchar(1);
alter table clients modify column email varchar(255);
alter table clients rename column email to e_mail;
alter table clients drop column age, drop column e_mail;



select
	cl.first_name as 'client name', cl.last_name as 'client surname',
    o.name as 'offer name',
    r.name as 'room name', r.price as 'room price'
from clients cl
	join offers o on cl.id = o.client_id
    join offer_rooms on o.id = offer_rooms.offer_id
    join rooms r on r.id = offer_rooms.room_id
;



select * from clients c 
left join offers o on c.id = o.client_id;

select * from clients c 
right join offers o on c.id = o.client_id;

select * from clients c 
inner join offers o on c.id = o.client_id;

select * from clients c 
cross join offers o on c.id = o.client_id;



select
	cl.first_name as 'client name', cl.last_name as 'client surname', count(cl.id) as offers
from clients cl
	join offers o on cl.id = o.client_id
    join offer_rooms on o.id = offer_rooms.offer_id
    join rooms r on r.id = offer_rooms.room_id
group by cl.first_name, cl.last_name; 

select
	cl.first_name as 'client name', cl.last_name as 'client surname', count(cl.id) as offers
from clients cl
	join offers o on cl.id = o.client_id
    join offer_rooms on o.id = offer_rooms.offer_id
    join rooms r on r.id = offer_rooms.room_id
where cl.last_name like 'S%'
group by cl.first_name, cl.last_name
order by offers desc;

